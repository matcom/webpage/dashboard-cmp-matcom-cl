import React from 'react';
import Alert from '@mui/material/Alert';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CustomPlot from '../components/CustomPlot';
import { useMeteo } from '../providers/meteo';

const unpack = (rows, key) => rows.map((row) => row[key]);

export default function CardWindSpeed(){
    const { loading, serie } = useMeteo();
	const [chartData, setChartData] = React.useState();

	React.useEffect(() => {
        setChartData();
        if(serie.length > 0){
			let trace = {
				type: "scatter",
				mode: "lines",
				name: 'Velocidad Viento',
				x: unpack(serie, 'date'),
				y: unpack(serie, 'speed'),
			};
			setChartData({
				data: [trace],
				layout: { autosize: true },
			});
        }
    }, [serie]);

    return (
        <Card>
            <CardHeader
                titleTypographyProps={{ variant: "h6" }}
                title="Velocidad (m/s)"
            />
            {loading ? (
                <CardContent>
                    Cargando...
                </CardContent>
            ) : (
                (!chartData) ? (
                    <CardContent>
                        <Alert severity="info">No hay datos</Alert>
                    </CardContent>
                ): (
                    <CardContent sx={{ pt: 0, pl: 0, pr: 0 }}>
                        <CustomPlot
                            style={{ width: '100%', height: '100%' }}
                            useResizeHandler
                            data={chartData.data}
                            layout={chartData.layout}
                        />
                    </CardContent>
                )
            )}
        </Card>
    )
}