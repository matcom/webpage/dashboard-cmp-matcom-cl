import React from 'react';
import Alert from '@mui/material/Alert';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CustomPlot from '../components/CustomPlot';
import { useMeteo } from '../providers/meteo';

const unpack = (rows, key) => rows.map((row) => row[key]);

export default function CardPbhl(){
    const { loadingPbhl, pbhl } = useMeteo();
	const [chartData, setChartData] = React.useState();

	React.useEffect(() => {
        setChartData();
        if(pbhl.length > 0){
			let trace = {
				type: "scatter",
				mode: "lines",
				name: 'PBHL',
				x: unpack(pbhl, 'date'),
				y: unpack(pbhl, 'value'),
			};
			setChartData({
				data: [trace],
				layout: { autosize: true },
			});
        }
    }, [pbhl]);

    return (
        <Card>
            <CardHeader
                titleTypographyProps={{ variant: "h6" }}
                title="PHBL"
            />
            {loadingPbhl ? (
                <CardContent>
                    Cargando...
                </CardContent>
            ) : (
                (!chartData) ? (
                    <CardContent>
                        <Alert severity="info">No hay datos</Alert>
                    </CardContent>
                ): (
                    <CardContent sx={{ pt: 0, pl: 0, pr: 0 }}>
                        <CustomPlot
                            style={{ width: '100%', height: '100%' }}
                            useResizeHandler
                            data={chartData.data}
                            layout={chartData.layout}
                        />
                    </CardContent>
                )
            )}
        </Card>
    )
}